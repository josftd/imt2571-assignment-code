<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            
            // Create PDO connection
            try{ 
            $this->db = new PDO('mysql:host=localhost;dbname=test', 'root','');
            }
            catch(PDOException $e){
                echo "error";
                $e->getMessage();
            }
    
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        
        try{ 
        $req = $this->db->prepare("SELECT * FROM book");
        $req->execute();
		$booklist = array();
        $res = $req->fetchAll(PDO::FETCH_ASSOC);
        if($res){
            
            foreach($res as $id=>$re) {
            
                $booklist[$id] = new Book($re["title"], $re["author"], $re["description"], $re["id"]);
            }
             
            return $booklist;            
        }   
   
        }catch(PDOException $e){
             echo "error";
             $e->getMessage();
           }
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        try{ 
        $req = $this->db->prepare("SELECT * FROM book WHERE id = :id");
        $req->bindParam(":id", $id, PDO::PARAM_INT);
        $req->execute();
        $res = $req->fetch(PDO::FETCH_ASSOC);
        if($res){    
            $book = new Book($res["title"], $res["author"], $res["description"], $res["id"]);
            return $book;
        }
            
        } catch(PDOException $e){
            echo "error";
            $e->getMessage();
                
        }
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
         try{ 
        $req = $this->db->prepare("INSERT INTO book (title, author, description) VALUES (:t, :a, :d)");
        $req->bindValue(':t', $book->title, PDO::PARAM_STR);
        $req->bindValue(':a', $book->author, PDO::PARAM_STR);
        $req->bindValue(':d', $book->description, PDO::PARAM_STR);
    if(!empty($book->title) && !empty($book->author)){ 
        $req->execute();
    
        }
             $book->id = $this->db->lastInsertId();
         }
        
            catch(PDOException $e){
                echo "error";
                $e->getMessage();
            }   
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        $id = $book->id;
        $book->title = $_POST['title'];
        $book->author = $_POST['author'];
        $book->description = $_POST['description'];
        if ($book->title != "" && $book->author != "") { 
            try { 
                $modify = $this->db->prepare("UPDATE book SET title = :title, author = :author, description = :description WHERE id =$id");
        
                // $modify->bindValue(':id', $book->id, PDO::PARAM_INT);
                $modify->bindValue(':title', $book->title, PDO::PARAM_STR);
                $modify->bindValue(':author', $book->author, PDO::PARAM_STR);
                $modify->bindValue(':description', $book->description, PDO::PARAM_STR);
                $modify->execute(); 
            }
            catch (PDOException $E) {
                echo "Error";
                $E->getMessage();
                $view = new ErrorView();
                $view->create();
            }
        }
        else {
            echo "Title and author can not be empty";
        }
    }
    
    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
       $req = $this->db->prepare("DELETE FROM  book WHERE id = :id");
       $req->bindValue(":id", $id, PDO::PARAM_INT);
       $req->execute();
       
        }
	
}

?>
